package app

import Game
import Rank
import com.zaxxer.hikari.HikariDataSource
import org.hibernate.Session
import org.hibernate.SessionFactory
import org.hibernate.boot.registry.StandardServiceRegistryBuilder
import org.hibernate.cfg.Configuration
import org.hibernate.cfg.Environment
import org.hibernate.context.internal.ThreadLocalSessionContext
import org.hibernate.dialect.H2Dialect

/**
 * Created by Riz
 */
object Config{


  val datasource : HikariDataSource by lazy(mode = LazyThreadSafetyMode.SYNCHRONIZED){
    val ds = HikariDataSource()
    ds.driverClassName = org.h2.Driver::class.java.canonicalName
    ds.username = "sa"
    ds.password = "riz1"
    ds.jdbcUrl = "jdbc:h2:./data/db/basketball;FILE_LOCK=NO"
    ds.maximumPoolSize = 1
    ds.minimumIdle = 1
    ds
  }

  val sessionFactory: SessionFactory by lazy(mode= LazyThreadSafetyMode.SYNCHRONIZED){
    val config = Configuration()
    config.addAnnotatedClass(Game::class.java)
    config.addAnnotatedClass(Rank::class.java)
    config.setProperty(Environment.DIALECT, H2Dialect::class.qualifiedName)
    config.setProperty(Environment.HBM2DDL_AUTO, "create")
    config.setProperty(Environment.CURRENT_SESSION_CONTEXT_CLASS, ThreadLocalSessionContext::class.qualifiedName);
    val serviceRegistry = StandardServiceRegistryBuilder()
                                  .applySetting(Environment.DATASOURCE, datasource)
                                  .applySettings(config.getProperties())
                                  .build();

    config.buildSessionFactory(serviceRegistry);
  }

  val session : Session by lazy(mode=LazyThreadSafetyMode.SYNCHRONIZED){
    sessionFactory.openSession()
  }
}

