import app.Config
import org.apache.commons.io.IOUtils
import org.apache.commons.lang3.StringUtils
import org.hibernate.Session
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import org.jsoup.select.Elements
import org.junit.Assert
import org.junit.Test
import java.io.File
import java.io.FileOutputStream
import java.io.InputStreamReader
import java.math.BigDecimal
import java.net.URL
import java.nio.charset.StandardCharsets
import java.time.LocalDate
import java.util.*
import java.util.regex.Pattern
import javax.persistence.*

@Entity
class Game{
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  var pk:Int = -1
  @Version var version : Long = 0
  @Column lateinit var date: LocalDate
  @Column(length = 3) var teamA : String = ""
  @Column var teamAScore : Int = -1
  @Column(length = 3) var teamB: String = ""
  @Column var teamBScore: Int = -1
  @Column(length = 1) var winner: String = ""
  @Column var teamALastPos: Int = -1
  @Column var teamBLastPos: Int = -1
}

@Entity
@Table(indexes = arrayOf(Index(name="team_idxs", columnList = "TEAM"), Index(name="date_idx", columnList = "DATE")))
class Rank{
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  var pk:Int = -1

  @Version
  var version : Long = 0
  @Column lateinit var date: LocalDate
  @Column(length = 1) var conference: String? = null
  @Column var pos: Int = -1
  @Column(length = 3) var team: String = ""
  @Column var wins: Int = -1
  @Column var losses: Int = -1
  @Column var winRate: BigDecimal = BigDecimal("-1.00")
  @Column var gamesBehind: BigDecimal = BigDecimal("-1.00")
  @Column var pointsFor: BigDecimal = BigDecimal("-1.00")
  @Column var pointsAgainst: BigDecimal = BigDecimal("-1.00")
}


fun extractData(startDate: LocalDate, endDate: LocalDate){
  var currentDate = startDate

  while(currentDate.isBefore(endDate)) {
    println("Writting data for : ${currentDate}")
    val year = currentDate.year
    val month = currentDate.monthValue.toString().padStart(2, '0')
    val day = currentDate.dayOfMonth.toString().padStart(2, '0')

    //parse page
    val doc = Jsoup.parse(File("data/pages/${year}${month}${day}.html"), StandardCharsets.UTF_8.name())

    //extract tables
    val games  = doc.select("table.no_highlight.stats_table.wide_table[style]")
    val ranks = doc.select("table.no_highlight.stats_table.wide_table[id]")

    if(games.size > 0){
      insertGames(games, currentDate)
      insertLeagues(ranks, currentDate);
    }
    currentDate = currentDate.plusDays(1)
  }
  getSession().flush()

  //addLastPosToGames()
}

fun insertGames(games: Elements, date: LocalDate){
  val pattern = Pattern.compile("[A-Z]{3}")
  games.forEach {
    val game = Game()
    game.date = date
    val trs = it.select("table tr")
    val row1 = trs.get(0).select("td")

    val teamAUrl = row1.get(0).select("a").attr("href")

    val teamAMatcher = pattern.matcher(teamAUrl)
    if(teamAMatcher.find()){
      game.teamA = teamAMatcher.group(0)
      game.teamAScore = Integer.parseInt(row1.get(1).text())
    }

    val row2 = trs.get(1).select("td")
    val teamBUrl = row2.get(0).select("a").attr("href")

    val teamBMatcher = pattern.matcher(teamBUrl)
    if(teamBMatcher.find()){
      game.teamB = teamBMatcher.group(0)
      game.teamBScore = Integer.parseInt(row2.get(1).text())
    }

    game.winner = if (game.teamAScore > game.teamBScore) "A" else "B"
    getSession().saveOrUpdate(game)
  }
}

fun getSession(): Session{
  return Config.session
}

fun insertLeagues(leagues: Elements, date: LocalDate){
  val eastLeague = leagues.get(2)
  val westLeague = leagues.get(3)

  insertRanks(eastLeague, "E", date)
  insertRanks(westLeague, "W", date)
}

fun insertRanks(league: Element, conference: String, date: LocalDate){
  val pattern = Pattern.compile("[A-Z]{3}")
  val rows = league.select("tbody tr")
  var ranks = ArrayList<Rank>()
  rows.forEach {
    val rank = Rank()
    rank.date = date
    rank.conference = conference
    val col = it.select("td")
    val href = col.get(0).select("a").attr("href")
    val matcher = pattern.matcher(href)
    if(matcher.find()){
      rank.team = matcher.group(0)
    }
    rank.wins = Integer.parseInt(col.get(1).text())
    rank.losses = Integer.parseInt(col.get(2).text())
    rank.winRate = BigDecimal(col.get(3).text())

    val gamesBehindStr = col.get(4).text()
    rank.gamesBehind = if(StringUtils.isNumeric(gamesBehindStr)) BigDecimal(gamesBehindStr) else BigDecimal(0.0)
    rank.pointsFor = BigDecimal(col.get(5).text())
    rank.pointsAgainst = BigDecimal(col.get(6).text())
    ranks.add(rank)
  }

  ranks.sort { a, b -> b.wins.compareTo(a.wins) }

  var pos = 0
  ranks.forEach { rank ->
    rank.pos = ++pos
    getSession().save(rank)
  }
}

fun posBeforeDate(team:String, date:LocalDate): Int{
  val pos = getSession().createSQLQuery("""
            SELECT POS FROM Rank where team = :team AND date < :date
            ORDER BY date desc
            """)
            .setParameter("team", team)
            .setParameter("date", date)
            .setMaxResults(1)
            .uniqueResult() as Int?

  return pos ?: -1
}


fun addLastPosToGames(){
  val games = getSession().createQuery("FROM Game").list() as List<Game>
  games.forEach { game ->
    println("Updating last pos for game: ${game.pk}")
    game.teamALastPos = posBeforeDate(game.teamA, game.date)
    game.teamBLastPos = posBeforeDate(game.teamB, game.date)
    getSession().saveOrUpdate(game)
  }
  getSession().flush()
}


fun fetchPage(url: String): String {
  var page = ""
  for (i in 0..5) {
    try {
      val pageUrl = URL(url);
      val conn = pageUrl.openConnection()
      conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 5.1; rv:19.0) Gecko/20100101 Firefox/19.0")
      conn.connect()
      val pageIn = InputStreamReader(conn.inputStream)
      page = IOUtils.toString(pageIn)
      pageIn.close()
      return page
    }
    catch(aEx: Exception) {
      Thread.sleep(1000); //wait 10 secs
    }
  }
  return page
}


fun downloadPages(start: LocalDate, endDate: LocalDate){
  var currentDate = start
  while(currentDate.isBefore(endDate)) {
    val uri : String = "http://www.basketball-reference.com/boxscores/index.cgi" +
                       "?month=${currentDate.monthValue}" +
                       "&day=${currentDate.dayOfMonth}"   +
                       "&year=${currentDate.year}"

    val page = fetchPage(uri)
    val dayOfMonth = currentDate.dayOfMonth.toString().padStart(2, '0')
    val month = currentDate.monthValue.toString().padStart(2, '0')
    val outFileName = "data/pages/${currentDate.year}${month}${dayOfMonth}.html"

    IOUtils.write(page, FileOutputStream(outFileName))
    currentDate = currentDate.plusDays(1)
  }
}

class TestClass{
  @Test
  fun testExtractData(){
    extractData(LocalDate.of(2015, 10, 28), LocalDate.of(2016,1,23))
    addLastPosToGames()
  }

  @Test
  fun testPosBeforeDate(){
    extractData(LocalDate.of(2015, 10, 28), LocalDate.of(2016,1,23))
    val result = posBeforeDate("BOS", LocalDate.of(2015, 10, 31))
    println(result)
    Assert.assertNotNull(result)
  }


  @Test
  fun testAsLastPos(){
    addLastPosToGames()
  }

  @Test
  fun testDownloadPages(){
    //downloadPages(LocalDate.of(2015, 10, 28), LocalDate.now())
  }

  @Test
  fun testHibernate(){
    val games = getSession().createCriteria(Game::class.java).list()
    Assert.assertNotNull(games.isEmpty())
  }
}